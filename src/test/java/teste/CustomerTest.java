package teste;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.github.javafaker.Faker;

import page.CustomerPage;


public class CustomerTest {
	Faker faker = new Faker();
	CustomerPage customerPage = new CustomerPage();

	@Before
	public void openApplication() {		
		customerPage.setup();

	}
	
	@Category(CustomerTest.class)
	@Test
	public void desafio1() throws InterruptedException {
		int opcao = 4; // [1]Bootstrap V3 Theme [2]"Bootstrap V4 Theme"
		String msg_expected = "Your data has been successfully stored into the database.";
		String url = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";

		String name = faker.name().firstName(); 
		String lastName = "Costa";
		String contactFirstName = "Jhonatan";
		String phone = "61 9999-9999";
		String addressLine1 = "Asa Sul, 0001";
		String addressLine2 = "SRTV";
		String city = "Brasilia";
		String state = "DF";
		String postalCode = "70000-000";
		String country = "Brasil";
		int employeer = 8;
		String creditLimit = "100";
		
		customerPage.openPage(url);
		
		customerPage.updateVersion(opcao);
		
		customerPage.addCustomer(name, lastName, contactFirstName, phone,
				addressLine1, addressLine2, city, state, postalCode, country,
				employeer, creditLimit);
		customerPage.saveCustomer();
		
		assertEquals(customerPage.getMenssageSaveCustomerSuccess().substring(0, 57), msg_expected);


	}
	
	
	@Category(CustomerTest.class)
	@Ignore
	public void desafio2() throws InterruptedException {
		int opcao = 4; // [1]Bootstrap V3 Theme [2]"Bootstrap V4 Theme"
		String msg_DeleteItens = "Are you sure that you want to delete this 1 item?";
		String msg_ConfirmDelete = "Your data has been successfully deleted from the database.";
		
		String url = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";

	
		String name = faker.name().firstName(); 
		String lastName = "Costa";
		String contactFirstName = "Jhonatan";
		String phone = "61 9999-9999";
		String addressLine1 = "Asa Sul, 0001";
		String addressLine2 = "SRTV";
		String city = "Brasilia";
		String state = "DF";
		String postalCode = "70000-000";
		String country = "Brasil";
		int employeer = 8;
		String creditLimit = "100";
		
		customerPage.openPage(url);
		customerPage.updateVersion(opcao);
		customerPage.addCustomer(name, lastName, contactFirstName, phone,
				addressLine1, addressLine2, city, state, postalCode, country,
				employeer, creditLimit);
		
		customerPage.saveAndGoBackList();
		customerPage.searchCustomer(name);
		customerPage.deleteCustomer();
		assertEquals(msg_DeleteItens,customerPage.msgDeleteCustomer());
		customerPage.confirmButtonDeleteCustomer();
		assertEquals(msg_ConfirmDelete,customerPage.getMenssageDeleteCustomerSuccess());
		
	}
	
	
	@After
	public void closeApplication(){
		customerPage.close();
	}

}